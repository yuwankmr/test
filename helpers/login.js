exports.login = async function (page, timeout, typingSpeed) {
    await page.waitForSelector('input[id="email"]', {
        timeout: `${timeout}`,
    });
    await page.waitForSelector('input[id="password"]', {
        timeout: `${timeout}`,
    });
    await page.type('input[id="email"]', "admin@ezey.net", {
        delay: typingSpeed,
    });
    await page.type('input[id="password"]', "12345678", {
        delay: typingSpeed,
    });
    await expect(page).toClick("button", {
        text: "Login",
        timeout: `${timeout}`,
    });
    await page.waitForNavigation({ waitUntil: "load" });
    await page.waitFor(2000);
};
