exports.addLicences = async function(page,timeout,typingSpeed,issueDatePosition,issueDateChangeYear,issueDateChangeMonth,issueDateChangeYearNumberOfTimes,issueDateChangeMonthNumberOfTimes,issueDateMonth,issueDateDay,expiryDatePosition,expiryDateChangeYear,expiryDateChangeMonth,expiryDateChangeYearNumberOfTimes,expiryDateChangeMonthNumberOfTimes,expiryDateMonth,expiryDateDay,
                                     driverDDLElement,driverDDLElementText,driverDDLOptionElement,driverDDLOptionElementTxt,licenseTypeDDLElement,licenseTypeDDLElementText,licenseTypeDDLOptionElement,licenseTypeDDLOptionElementTxt,
                                     countryDDLElement,countryDDLElementText,countryDDLOptionElement,countryDDLOptionElementTxt,
                                     regionDDLElement,regionDDLElementText,regionDDLOptionElement,regionDDLOptionElementTxt,
                                     licenseNumber,licenseClass,endorsements,conditions,notes) {
     var buttonText='Add License';
    // page.setDefaultTimeout(timeout);
     var buttonText='Add License';
     await expect(page).toClick('button', { text:`${buttonText}`, timeout: `${timeout}`})
     await page.waitFor(1000);
     console.log("Current page:", page.url());
     var setElementValue=require("./setElementValue");

     await setElementValue.setCalenderPopUpValue(page,timeout,issueDatePosition,issueDateChangeYear,issueDateChangeMonth,issueDateChangeYearNumberOfTimes,issueDateChangeMonthNumberOfTimes,issueDateMonth,issueDateDay);
     await page.waitFor(1000);
     await setElementValue.setCalenderPopUpValue(page,timeout,expiryDatePosition,expiryDateChangeYear,expiryDateChangeMonth,expiryDateChangeYearNumberOfTimes,expiryDateChangeMonthNumberOfTimes,expiryDateMonth,expiryDateDay);
  
     
      //Set Driver
      var setElementValue=require("./setElementValue");
      await setElementValue.setValueInDropDownList(page,timeout,driverDDLElement,driverDDLElementText,driverDDLOptionElement,driverDDLOptionElementTxt);
    
     //Set license type
     await setElementValue.setValueInDropDownList(page, timeout,licenseTypeDDLElement,licenseTypeDDLElementText,licenseTypeDDLOptionElement,licenseTypeDDLOptionElementTxt);
            
      //Select Country 
      await setElementValue.setValueInDropDownList(page,timeout,countryDDLElement,countryDDLElementText,countryDDLOptionElement,countryDDLOptionElementTxt);
     

      //Select Region 
      await setElementValue.setValueInDropDownList(page,timeout,regionDDLElement,regionDDLElementText,regionDDLOptionElement,regionDDLOptionElementTxt);
     

      //enter Licence number
      await page.waitForSelector('input[id="number"]', {
       timeout: `${timeout}` });
      await page.type('input[id="number"]',licenseNumber, {delay: typingSpeed});
      await page.waitFor(1000);
      
       //enter class 
       await page.waitForSelector('input[id="class"]', {timeout: `${timeout}` });
       await page.type('input[id="class"]',licenseClass, {delay: typingSpeed});
       await page.waitFor(1000);

        //enter endorsements
        await page.waitForSelector('input[id="endorsements"]', {timeout: `${timeout}` });
        await page.type('input[id="endorsements"]',endorsements, {delay: typingSpeed});
        await page.waitFor(1000);

           //enter conditions
        await page.waitForSelector('input[id="conditions"]', {timeout: `${timeout}` });
        await page.type('input[id="conditions"]',conditions, {delay: typingSpeed});
        await page.waitFor(1000);
        
        //enter conditions
        await page.waitForSelector('input[id="notes"]', {timeout: `${timeout}` });
        await page.type('input[id="notes"]',notes, {delay: typingSpeed});
        await page.waitFor(1000);

        await expect(page).toClick('button', { text:'Add License', timeout: `${timeout}`})
        await page.waitFor(3000);

     
       
       
    
};