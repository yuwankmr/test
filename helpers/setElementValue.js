//const escapeXpathString = str => {
// const splitedQuotes = str.replace(/'/g, `', "'", '`);
// return `concat('${splitedQuotes}', '')`;
//};

// Types value inside input boxeZZZ
exports.type = async function (page, element, value, timeout, typingSpeed) {
    await page.waitForSelector(element, {
        timeout: `${timeout}`,
    });
    await page.type(element, value, {
        delay: typingSpeed,
    });
};

exports.timeselector = async function (
    page,
    position,
    hour,
    placeholder = "Pick a Time"
) {
    var timeselect = await page.$x(`//input[@placeholder="${placeholder}"]`);
    await timeselect[position].click();
    await page.waitFor(1000);
    var hours = await page.$x(`//span[@class="rdtBtn"]`);
    for (let click = 0; click < hour; click++) {
        await hours[position * 4].click();
        // 0 -> 0, 1->4,2 ->8
    }
    await page.waitFor(1000);
    // await page.$eval('input[@placeholder="Pick a Time"]', (e) =>
    //     e[position].blur()
    // );
};

exports.clickByTextUsingXpath = async function (page, text, element) {
    const escapedText = escapeXpathString(text);
    const linkHandlers = await page.$x(
        "//" + element + "[contains(text()," + escapedText + " )]"
    );
    console.log("Link nn " + linkHandlers[0].text);
    if (linkHandlers.length > 0) {
        await linkHandlers[0].click();
    } else {
        throw new Error(`Link not found: ${text}`);
    }
};

//This function sets the value of materialUI Drop Down List.

exports.setValueInDropDownList = async function (
    page,
    timeoutValue,
    ddselectElement,
    ddselectElementTxt,
    optionElement,
    optionElementValue
) {
    await page.waitFor(2000);
    await expect(page).toClick(`${ddselectElement}`, {
        text: `${ddselectElementTxt}`,
        timeout: `${timeoutValue}`,
    });
    await page.waitFor(1000);
    // var selectOption=  await page.$x(`//${optionElement}[contains(normalize-space(string()) ,'${optionElementValue}')]`);
    // await selectOption[0].click();
    await expect(page).toClick(`${optionElement}`, {
        text: `${optionElementValue}`,
        timeout: `${timeoutValue}`,
    });
    await page.waitFor(1000);
};
// This function is set the value of a  calander pop up used by the following
//https://demos.creative-tim.com/material-dashboard-pro-react/#/documentation/calendar

//If ChangeYear is set to 1  to advance the year. >1 means advance year ,0 means dont change year and <0 means to change the year to a previous year
//ChangeMonth is set to 1 in order to advance the month >1 means advance the month by DateChangeYearNumberOfTimes times. 0 means dont change the month and <0 means to change the month DateChangeMonthNumberOfTimes  times  backwards
// position= 0 indicates that this the first date field in the page  according to the order of appearence.
//position= 1 indicates that this the second date field in the page  according to the order of appearence.
exports.setCalenderPopUpValue = async function (
    page,
    timeoutValue,
    position,
    changeYear,
    changeMonth,
    changeYearNumberOfTimes,
    changeMonthNumberOfTimes,
    month,
    day,
    adjestment = 0
) {
    // test start dated and expiry date rdtPrev rdtSwitch

    var classValue = null;
    var next = null;
    //  page.waitForXpath( not working  https://github.com/puppeteer/puppeteer/issues/3235
    // it says upgrade pupeter to 1.8.0 but this project uses 2.0.0 and still not working
    // so using waitForSelector intead

    //await page.waitForXpath('//input[@placeholder="Pick a Date"]');
    await page.waitForSelector('input[placeholder="Pick a Date"]', {
        timeout: `${timeoutValue}`,
    });
    var date = await page.$x('//input[@placeholder="Pick a Date"]');

    await date[position].click();
    await page.waitFor(1000);

    if (changeYear > 0) {
        classValue = "rdtNext";
    } else if (changeYear < 0) {
        classValue = "rdtPrev";
    }

    if (changeYear != 0) {
        await page.waitForSelector('th[class="rdtSwitch"]', {
            timeout: `${timeoutValue}`,
        });
        var switchYear = await page.$x(`//th[@class="rdtSwitch"]`);
        await switchYear[position + adjestment].click();
        await page.waitFor(1000);
        console.log("changeYearNumberOfTimes    " + changeYearNumberOfTimes);
        for (let step = 0; step < changeYearNumberOfTimes; step++) {
            // advance or go back year
            await page.waitForSelector(`th[class="${classValue}"]`, {
                timeout: `${timeoutValue}`,
            });
            next = await page.$x(`//th[@class="${classValue}"]`);
            await next[position + adjestment].click();
            await page.waitFor(1000);
        } //end for

        // select month view after changing year
        await page.waitForSelector('td[class="rdtMonth"', {
            timeout: `${timeoutValue}`,
        });
        var selectMonth = await page.$x(
            `//td[@class="rdtMonth" and contains(string(), '${month}')]`
        );
        await selectMonth[0].click();
        await page.waitFor(1000);
    } //end if  changeYear

    if (changeMonth > 0) {
        classValue = "rdtNext";
    } //if(changeMonth)
    else if (changeMonth < 0) {
        classValue = "rdtPrev";
    } //else if

    if (changeMonth != 0) {
        for (let step = 0; step < changeMonthNumberOfTimes; step++) {
            // advance or go back a month
            await page.waitForSelector(`th[class="${classValue}"]`, {
                timeout: `${timeoutValue}`,
            });
            next = await page.$x(`//th[@class="${classValue}"]`);
            await next[position + adjestment].click();
            await page.waitFor(1000);
        } //end for
    } //end if (changeMonth!=0)

    var calanderPopUpDay2 = await page.$x(`//td[contains(text(), ${day})]`);
    //('input[placeholder="Pick a Date"]');

    await calanderPopUpDay2[position].click();
    await page.waitFor(1000);
    // await page.$eval('input[@placeholder="Pick a Date"]"]', (e) =>
    //     e[position].blur()
    // );
};
