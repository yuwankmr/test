const timeout = 100000000;
// typingSpeed value is set for wait time between keystrokes. Simulates realistic typing.
const typingSpeed = 50;
require("expect-puppeteer");

describe(
    "access site http://dev4.ez-net.local:3000/",
    () => {
        let page;
        beforeAll(async () => {
            jest.setTimeout(timeout);
            page = await global.__BROWSER__.newPage();
            await page.goto("http://dev4.ez-net.local:3000", {
                timeout: timeout,
            });
        }, timeout);

        afterEach(async () => {
            await page.waitFor(1000);
        });

        afterAll(async () => {
            await page.close();
        });

        it("Login to site", async () => {
            require("../helpers/login").login(page, timeout, typingSpeed);
        });

        it("View Customers", async () => {
            await page.waitFor(2000);
            await expect(page).toClick("li.Sidebar-item-130 a", {
                text: "Customer",
                timeout: `${timeout}`,
            });
            await page.waitFor(2000);
            await expect(page).toClick("li.Sidebar-item-130 a", {
                text: "Customer",
                timeout: `${timeout}`,
            });
            await expect(page).toClick("a.Sidebar-collapseItemLink-143", {
                text: "Customer",
                timeout: `${timeout}`,
            });
            await page.waitForNavigation({ waitUntil: "load" });
            await page.waitFor(1000);
        });

        it("Fill Customer Details Form", async () => {
            await expect(page).toClick("button", {
                text: "Add Customer",
                timeout: `${timeout}`,
            });

            var setElementValue = require("../helpers/setElementValue");

            async function typer(element, value) {
                await setElementValue.type(
                    page,
                    element,
                    value,
                    timeout,
                    typingSpeed
                );
            }
            await typer('input[type="email"]', "someone@somewhere.com");
            await typer('input[id="password"]', "12345678");
            await typer('input[id="firstName"]', "John");
            await typer('input[id="lastName"]', "Smith");
            await typer('input[id="addressLine1"]', "No. XXX");
            await typer('input[id="addressLine2"]', "A Street");
            await typer('input[id="city"]', "Some City");
            await setElementValue.setValueInDropDownList(
                page,
                timeout,
                "label",
                "Select Province *",
                "li",
                "Ontario"
            );
            await typer('input[id="postalCode"]', "00130");
            await typer('input[id="businessPhone"]', "+94xx-xxx-xxxx");
            await typer('input[id="homePhone"]', "+94xx-xxx-xxxx");
            await typer('input[id="cellPhone"]', "+94xx-xxx-xxxx");
            await typer('input[id="userNotes"]', "Nothing to note");
            await expect(page).toClick("span", {
                text: "Active Status",
                timeout: `${timeout}`,
            });
        });

        it("Click Add Customer Button", async () => {
            // await expect(page).toClick("button", {
            //     text: "Add Customer",
            //     timeout: `${timeout}`,
            // });
            // await page.waitForNavigation({ waitUntil: "load" });
            await page.waitFor(4000);
        });
    },
    timeout
);
