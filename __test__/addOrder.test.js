const timeout = 100000000;
// typingSpeed value is set for wait time between keystrokes. Simulates realistic typing.
const typingSpeed = 50;
require("expect-puppeteer");

describe(
    "access site http://dev4.ez-net.local:3000/",
    () => {
        let page;
        beforeAll(async () => {
            jest.setTimeout(timeout);
            page = await global.__BROWSER__.newPage();
            await page.goto("http://dev4.ez-net.local:3000", {
                timeout: timeout,
            });
        }, timeout);

        afterEach(async () => {
            await page.waitFor(1000);
        });

        afterAll(async () => {
            await page.close();
        });

        it("Login to site", async () => {
            require("../helpers/login").login(page, timeout, typingSpeed);
        });

        it("View Orders", async () => {
            await page.waitFor(3000);
            await expect(page).toClick("li.Sidebar-item-130 a", {
                text: "Order Management",
                timeout: `${timeout}`,
            });
            await page.waitFor(2000);
            await expect(page).toClick("li.Sidebar-item-130 a", {
                text: "Order Management",
                timeout: `${timeout}`,
            });
            await page.waitFor(2000);
            await expect(page).toClick("a.Sidebar-collapseItemLink-143", {
                text: "Order Info",
                timeout: `${timeout}`,
            });
            // await page.waitForNavigation({ waitUntil: "load" });
            await page.waitFor(1000);
        });

        it("ADD Order", async () => {
            await expect(page).toClick("button", {
                text: "Add Order",
                timeout: `${timeout}`,
            });

            var setElementValue = require("../helpers/setElementValue");

            async function typer(element, value) {
                await setElementValue.type(
                    page,
                    element,
                    value,
                    timeout,
                    typingSpeed
                );
            }
            async function selector(option, value, element = "label") {
                await setElementValue.setValueInDropDownList(
                    page,
                    timeout,
                    element,
                    option,
                    "li",
                    value
                );
            }
            await selector("Select Name *", "User 1 Test");
            await typer('input[name="phone"]', "+94XX XXX XXXX");
            await typer('input[name="oi_reference"]', "References");

            await typer('input[name="pa_full_addr"]', "No.32");
            await typer('input[name="pa_location_name"]', "Location Name");
            await typer('input[name="pa_street"]', "Some Street");
            await typer('input[name="pa_unit"]', "51");
            await typer('input[name="pa_postal_code"]', "0090");
            await typer('input[name="pa_city"]', "City");
            await selector("Select Province *", "Ontario");
            await typer('input[name="pa_pickup_contact_name"]', "Pickup Cname");
            await typer('input[name="pa_pickup_contact_phone"]', "0112233445");
            await typer('input[name="pa_pickup_contact_ph"]', "contactPH");
            await selector("Select Zone *", "N/A");
            await setElementValue.timeselector(page, 0, 2);
            await typer('input[name="pa_dispatch"]', "Dispatch");
            await typer('input[name="pa_notes"]', "Notes");
            await typer('input[name="pa_closed_days_notes"]', "CDNotes");

            // Delivery Details

            await typer('input[name="da_full_addr"]', "No.32");
            await typer('input[name="da_location_name"]', "Location Name");
            await typer('input[name="da_street"]', "Some Street");
            await typer('input[name="da_unit"]', "51");
            await typer('input[name="da_postal_code"]', "0090");
            await typer('input[name="da_city"]', "City");
            await selector(
                "",
                "Ontario",
                "div[id='mui-component-select-da_province_id']"
            );
            await typer(
                'input[name="da_delivery_contact_name"]',
                "Pickup Cname"
            );
            await typer(
                'input[name="da_delivery_contact_phone"]',
                "0112233445"
            );
            await typer('input[name="da_delivery_contact_ph"]', "contactPH");
            await selector(
                "Select Zone *",
                "N/A",
                'label[for="da_zone_id-id"]'
            );
            await setElementValue.timeselector(page, 1, 2);
            await typer('input[name="da_dispatch"]', "Dispatch");
            await typer('input[name="da_notes"]', "Notes");
            await typer('input[name="da_closed_days_notes"]', "CDNotes");

            // // Services

            await setElementValue.setCalenderPopUpValue(
                page,
                timeout,
                1,
                1,
                1,
                3,
                3,
                "Feb",
                23
            );
            // await setElementValue.timeselector(page, 4, 2);
            await setElementValue.timeselector(page, 3, 2);
            await setElementValue.timeselector(page, 2, 2);
            await setElementValue.setCalenderPopUpValue(
                page,
                timeout,
                0,
                1,
                1,
                2,
                3,
                "Mar",
                10,
                0
            );
            await selector("Select Service Type *", "Order Service Test");
            await selector("Select Vehicle Type *", "Order Testing 1");
            await selector("Select Status *", "On Going");

            await typer(
                'textarea[name="oi_order_information"]',
                "Order Information"
            );
            await typer('input[name="oi_type"]', "3");
            await typer('input[name="oi_quantity"]', "7");
            await typer('textarea[name="oi_instructions"]', "Instructions");

            await selector(
                "",
                "Order Truck 1",
                "div[id='mui-component-select-trailer']"
            );
            await typer('input[name="oi_contact_name"]', "Contact Name");
            await typer('input[name="oi_contact_number"]', "0112233445");
            await typer('input[name="oi_total_value"]', "1000");
            await selector(
                "Canadian Dollar",
                "US Dollar",
                "div[id='mui-component-select-oi_total_value_currency']"
            );
            await expect(page).toClick("span", {
                text: "Req Insurance *",
                timeout: `${timeout}`,
            });
            await expect(page).toClick("span", {
                text: "Cash on Delivery",
                timeout: `${timeout}`,
            });

            // Package
            await typer('input[name="p_weight"]', "135");
            await typer('input[name="p_length"]', "100");
            await typer('input[name="p_height"]', "150");
            await typer('input[name="p_width"]', "220");
            await typer('input[name="p_volume"]', "70");
            await typer('input[name="p_sctg"]', "01001");
            await typer('input[name="p_hs"]', "3");
            await typer('input[name="p_scg"]', "8");
            // await setElementValue.timeselector(page, 5, 3);
            // await setElementValue.timeselector(page, 4, 2);
            await typer('input[name="p_pickup_contact_name"]', "Contact name");
            await typer('input[name="p_pickup_contact_phone"]', "0778812345");
            await selector(
                "Select Status",
                "On Going",
                "label[for='p_status-id']"
            );

            // await expect(page).toClick("button", {
            //     text: "Add Order",
            //     timeout: `${timeout}`,
            // });
            await page.waitFor(3000);

            // await page.goto('http://dev4.ez-net.local:3000/admin/view-training/');
        });
    },
    timeout
);
