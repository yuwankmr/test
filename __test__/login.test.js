const timeout = 10000;
// typingSpeed value is set for wait time between keystrokes. Simulates realistic typing.
const typingSpeed = 50;
require("expect-puppeteer");

describe(
    "access site http://dev4.ez-net.local:3000/",
    () => {
        let page;
        beforeAll(async () => {
            jest.setTimeout(timeout);
            page = await global.__BROWSER__.newPage();
            await page.goto("http://dev4.ez-net.local:3000");
        }, timeout);

        afterEach(async () => {
            await page.waitFor(1000);
        });

        afterAll(async () => {
            await page.close();
        });

        it("Input login credentials", async () => {
            await page.waitForSelector('input[id="email"]');
            await page.waitForSelector('input[id="password"]');
            await page.type('input[id="email"]', "admin@ezey.net", {
                delay: typingSpeed,
            });
            await page.type('input[id="password"]', "12345678", {
                delay: typingSpeed,
            });
        });

        it("Login to site", async () => {
            await expect(page).toClick("button", { text: "Login" });

            //const dialog = await expect(page).toDisplayDialog(async () => {
            // await expect(page).toClick('button', { text: 'Login' })
            //})

            //await expect(page.waitForSelector('iziToast-wrapper',{message:'sucessfull'})).toEqual('successful');

            await page.waitFor(1000);
        });
    },
    timeout
);
