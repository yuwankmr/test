const timeout = 100000000;
// typingSpeed value is set for wait time between keystrokes. Simulates realistic typing.
const typingSpeed = 50;
require("expect-puppeteer");

describe(
    "access site http://dev4.ez-net.local:3000/",
    () => {
        let page;
        beforeAll(async () => {
            jest.setTimeout(timeout);
            page = await global.__BROWSER__.newPage();
            await page.goto("http://dev4.ez-net.local:3000", {
                timeout: timeout,
            });
        }, timeout);

        afterEach(async () => {
            await page.waitFor(1000);
        });

        afterAll(async () => {
            await page.close();
        });

        it("Login to site", async () => {
            require("../helpers/login").login(page, timeout, typingSpeed);
        });

        it("View Trailers", async () => {
            await page.waitFor(3000);
            await expect(page).toClick("li.Sidebar-item-130 a", {
                text: "Administration",
                timeout: `${timeout}`,
            });
            await page.waitFor(2000);
            await expect(page).toClick("li.Sidebar-item-130 a", {
                text: "Administration",
                timeout: `${timeout}`,
            });
            await expect(page).toClick("a", {
                text: "Trailer",
                timeout: `${timeout}`,
            });
            await page.waitForNavigation({ waitUntil: "load" });
            await page.waitFor(1000);
        });

        it("ADD Trailer", async () => {
            await expect(page).toClick("button", {
                text: "Add Trailer",
                timeout: `${timeout}`,
            });

            var setElementValue = require("../helpers/setElementValue");

            async function typer(element, value) {
                await setElementValue.type(
                    page,
                    element,
                    value,
                    timeout,
                    typingSpeed
                );
            }
            async function selector(option, value) {
                await setElementValue.setValueInDropDownList(
                    page,
                    timeout,
                    "label",
                    option,
                    "li",
                    value
                );
            }
            await selector("Select Type *", "Long Truck");
            await typer('input[id="unitNumber"]', "51");
            await typer('input[id="trailerName"]', "Truck");

            var yearselector = await page.$x(
                '//input[@placeholder="Pick a Year"]'
            );
            await yearselector[0].click();
            await page.waitFor(1000);
            var yeartoclick = await page.$x(`//td[contains(text(), 2025)]`);
            await yeartoclick[0].click();
            await page.waitFor(1000);

            await selector("Select Make *", "Audi");
            await selector("Select Model *", "A8");
            await typer('input[id="owner"]', "Owner");
            await typer('input[id="plateNumber"]', "1234");
            await typer('input[id="plateOwner"]', "PlateOwner");
            await typer('input[id="vinNumber"]', "12345");

            await setElementValue.setCalenderPopUpValue(
                page,
                timeout,
                0,
                1,
                1,
                2,
                3,
                "Mar",
                10,
                1
            );
            await typer('input[id="lessee"]', "Lessee");
            await typer('input[id="trailerLength"]', "10 Feet");
            await typer('input[id="tireSize"]', "235-75R-22.5");
            await typer('input[id="tareWeight"]', "295");
            await setElementValue.setCalenderPopUpValue(
                page,
                timeout,
                1,
                1,
                1,
                3,
                3,
                "Feb",
                23,
                1
            );
            await expect(page).toClick("span", {
                text: "Active Status",
                timeout: `${timeout}`,
            });
            await typer('input[id="notes"]', "Notes");

            // await expect(page).toClick("button", {
            //     text: "Add Trailer",
            //     timeout: `${timeout}`,
            // });
            await page.waitFor(3000);

            // await page.goto('http://dev4.ez-net.local:3000/admin/view-training/');
        });
    },
    timeout
);
