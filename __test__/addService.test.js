const timeout = 100000000;
// typingSpeed value is set for wait time between keystrokes. Simulates realistic typing.
const typingSpeed = 50;
require("expect-puppeteer");

describe(
    "access site http://dev4.ez-net.local:3000/",
    () => {
        let page;
        beforeAll(async () => {
            jest.setTimeout(timeout);
            page = await global.__BROWSER__.newPage();
            await page.goto("http://dev4.ez-net.local:3000", {
                timeout: timeout,
            });
        }, timeout);

        afterEach(async () => {
            await page.waitFor(1000);
        });

        afterAll(async () => {
            await page.close();
        });

        it("Login to site", async () => {
            require("../helpers/login").login(page, timeout, typingSpeed);
        });

        it("View Trailers", async () => {
            await page.waitFor(3000);
            await expect(page).toClick("li.Sidebar-item-130 a", {
                text: "Maintenance",
                timeout: `${timeout}`,
            });
            await page.waitFor(2000);
            await expect(page).toClick("li.Sidebar-item-130 a", {
                text: "Maintenance",
                timeout: `${timeout}`,
            });
            await expect(page).toClick("a", {
                text: "Servicing",
                timeout: `${timeout}`,
            });
            await page.waitForNavigation({ waitUntil: "load" });
            await page.waitFor(1000);
        });

        it("ADD Trailer", async () => {
            await expect(page).toClick("button", {
                text: "Add Service",
                timeout: `${timeout}`,
            });

            var setElementValue = require("../helpers/setElementValue");

            async function typer(element, value) {
                await setElementValue.type(
                    page,
                    element,
                    value,
                    timeout,
                    typingSpeed
                );
            }
            async function selector(option, value) {
                await setElementValue.setValueInDropDownList(
                    page,
                    timeout,
                    "label",
                    option,
                    "li",
                    value
                );
            }
            // await selector("Select Type *", "Vehicle");
            await selector("Select Name *", "Order Test");
            await selector("Select Service Type *", "Order Service Test");
            await typer('input[id="odometer"]', "51");
            await typer('input[id="description"]', "description");
            await typer('input[id="refNumber"]', "#12345");
            await typer('input[id="billedHours"]', "4");
            await typer('input[id="labourCost"]', "1250");
            await typer('input[id="partsCost"]', "2000");
            await typer('input[id="totalCost"]', "3250");
            await setElementValue.setCalenderPopUpValue(
                page,
                timeout,
                0,
                1,
                1,
                2,
                3,
                "Mar",
                10
            );

            // await expect(page).toClick("button", {
            //     text: "Add Service",
            //     timeout: `${timeout}`,
            // });
            await page.waitFor(3000);

            // await page.goto('http://dev4.ez-net.local:3000/admin/view-training/');
        });
    },
    timeout
);
