const timeout = 100000000;
// typingSpeed value is set for wait time between keystrokes. Simulates realistic typing.
const typingSpeed = 50;
require("expect-puppeteer");

describe(
    "access site http://dev4.ez-net.local:3000/",
    () => {
        let page;
        beforeAll(async () => {
            jest.setTimeout(timeout);
            page = await global.__BROWSER__.newPage();
            await page.goto("http://dev4.ez-net.local:3000", {
                timeout: timeout,
            });
        }, timeout);

        afterEach(async () => {
            await page.waitFor(1000);
        });

        afterAll(async () => {
            await page.close();
        });

        it("Login to site", async () => {
            require("../helpers/login").login(page, timeout, typingSpeed);
        });

        it("View Permits", async () => {
            await page.waitFor(3000);
            await expect(page).toClick("li.Sidebar-item-130 a", {
                text: "Administration",
                timeout: `${timeout}`,
            });
            await page.waitFor(2000);
            await expect(page).toClick("li.Sidebar-item-130 a", {
                text: "Administration",
                timeout: `${timeout}`,
            });
            await expect(page).toClick("a.Sidebar-collapseItemLink-143", {
                text: "Permits",
                timeout: `${timeout}`,
            });
            // await page.waitForNavigation({ waitUntil: "load" });
            // await page.waitFor(1000);
        });

        it("ADD Permit", async () => {
            await expect(page).toClick("button", {
                text: "Add Permit",
                timeout: `${timeout}`,
            });

            var setElementValue = require("../helpers/setElementValue");

            async function typer(element, value) {
                await setElementValue.type(
                    page,
                    element,
                    value,
                    timeout,
                    typingSpeed
                );
            }
            async function selector(option, value) {
                await setElementValue.setValueInDropDownList(
                    page,
                    timeout,
                    "label",
                    option,
                    "li",
                    value
                );
            }
            // await selector("User Permit", "User Permit");
            await selector("Select User*", "test pacex");
            await typer('input[id="idNumber"]', "51");
            await typer('input[id="description"]', "Permit test 1");
            await setElementValue.setCalenderPopUpValue(
                page,
                timeout,
                0,
                1,
                1,
                2,
                3,
                "Mar",
                10
            );
            await setElementValue.setCalenderPopUpValue(
                page,
                timeout,
                1,
                1,
                1,
                3,
                3,
                "Feb",
                23
            );
            await typer('input[id="notes"]', "Notes");

            // await expect(page).toClick("button", {
            //     text: "Add Permit",
            //     timeout: `${timeout}`,
            // });
            await page.waitFor(3000);

            // await page.goto('http://dev4.ez-net.local:3000/admin/view-training/');
        });
    },
    timeout
);
